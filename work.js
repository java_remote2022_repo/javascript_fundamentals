const payElement = document.getElementById("pay");
const bankElement = document.getElementById("bank");
const workElement = document.getElementById("work");
const payLoanElement = document.getElementById("payLoan");

let pay = 0.0;
payLoanElement.hidden = true;

const handleWork = e => {
    pay += 100;
    payElement.innerHTML = `Pay: ${pay}$`;
}

const handleBank = e =>{
    if(activeLoan){
        loan -= (pay * 0.1);
        balance += (pay * 0.9);
        pay = 0;
        outStandingLoan.innerHTML = `Outstanding Loan: ${loan}$`;
        balanceElement.innerHTML = `Balance: ${balance}$`;
        payElement.innerHTML = `Pay: ${pay}$`;
    }else{
        balance += pay;
        pay = 0;
        balanceElement.innerHTML = `Balance: ${balance}$`;
        payElement.innerHTML = `Pay: ${pay}$`;
    }
    
}

const handlePayLoan = e =>{
    if(pay <= loan){
        loan -= pay;
        pay  = 0;
        if(loan === 0){
            activeLoan = false;
            payLoanElement.hidden = true;
        }
        outStandingLoan.innerHTML = `Outstanding Loan: ${loan}`;
        payElement.innerHTML = `Pay: ${pay}$`;
    }else{
        let temp = pay -loan;
        balance += temp;
        loan = 0;
        pay = 0;
        activeLoan = false;
        payLoanElement.hidden = true;
        outStandingLoan.innerHTML = `Outstanding Loan: ${loan}$`;
        balanceElement.innerHTML = `Balance: ${balance}$`;
        payElement.innerHTML = `Pay: ${pay}$`;
    }
    
}

workElement.addEventListener("click", handleWork);
bankElement.addEventListener("click", handleBank);
payLoanElement.addEventListener("click", handlePayLoan);