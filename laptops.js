const laptopsElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features");
const nameElement = document.getElementById("laptopName");
const descriptionElement = document.getElementById("description");
const priceElement = document.getElementById("price");
const buyElement = document.getElementById("buy");
const imageElement = document.getElementById("image");

let laptops = [];
let price = 0;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptops(laptops));


const addLaptops = (laptops) =>{
    laptops.forEach(x => addLaptop(x));
    featuresElement.innerHTML = laptops[0].specs;
    nameElement.innerHTML = laptops[0].title;
    descriptionElement.innerHTML = laptops[0].description;
    priceElement.innerHTML = `${laptops[0].price}$`;
    imageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${laptops[0].image}`;
    price = laptops[0].price;
}

const addLaptop = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopsElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

const handleLaptopChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    featuresElement.innerHTML = selectedLaptop.specs;
    nameElement.innerHTML = selectedLaptop.title;
    descriptionElement.innerHTML = selectedLaptop.description;
    priceElement.innerHTML = `${selectedLaptop.price}$`;
    imageElement.src = `https://noroff-komputer-store-api.herokuapp.com/${selectedLaptop.image}`;
    price = selectedLaptop.price;
}

const handleLaptopBuy = e => {
    if(balance < price){
        window.alert("Cannot afford that computer!");
    }else{
        window.alert("Congratulations! You are now the owner of this computer");
        console.log(price);
        console.log(balance);
        balance -= price; 
        balanceElement.innerHTML = `Balance: ${balance}$`;
    }
}

laptopsElement.addEventListener("change", handleLaptopChange);
buyElement.addEventListener("click", handleLaptopBuy);