const balanceElement = document.getElementById("balance");
const getLoanElement = document.getElementById("getLoan");
const outStandingLoan = document.getElementById("loan");


let balance = 200.00;
let loan = 0.0;
let activeLoan = false;

const handleGettingLoan = e => {
    if(activeLoan){
        window.alert("You alredy have a loan, you can only take out one loan at a time.")
    }else{
        let loanInput = prompt("Please enter how much loan you need:");
        if(loanInput != null && !isNaN(loanInput) && loanInput <= (balance*2)){
            loan += parseFloat(loanInput);
            outStandingLoan.innerHTML = `Outstanding Loan: ${loan}$`;
            balance += parseFloat(loanInput);
            balanceElement.innerHTML = `Balance: ${balance}$`;
            payLoanElement.hidden = false;
            activeLoan = true;
        }else{
            window.alert("Invalid loan value!")
        }
    }
}

getLoanElement.addEventListener("click", handleGettingLoan);