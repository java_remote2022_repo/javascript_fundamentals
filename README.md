# Computer Store JavaScript app

## The bank
---
In the bank panel a user can see their bank balance (on start it's 200$).
With the Get a Loan button, the user can take out a loan, to increase their balance,
after pushing the button, a prompt pops up on the screen, asking for the amount of loan they want. The amount can't be more than double of the users balance. Also a user can't take out another loan, if they alredy has an outstanding loan. After taking out a loan a new line appears, wich shows the amount of loan the user has.

## Work
---
In the work panel, the user can see how much pay did they get for working. A user can work by clicking the work button, each click adds 100$ to their pay. Pushing the bank button, all of the users pay gets transfered to their bank balance. If the users has a loan, a third Repay loan button appears, clicking that, the user transfers all their pay, but this time its goes to pay the loan first, any reamaining amount will be transfered to the balance.If there is a loan, the bank button also works differently, 10% of the pay would be payed towards the loan aotumaticly.

## Laptops
---
In the laptops panel, the users can look through the available laptops from a drop down menu. After selecting a laptop from the menu, it shows up under it, with the features and a picture and the price of the laptop. Pressing the buy now button will try to buy the laptop, if the user has enough bank balance, a pop up message will congratulate them on the purchase, if there is not enough balance, the massage will say they can't afford it.

## User Interface
---
![ui](compstoreUI.JPG)
## Maintainer
---
@tothbt